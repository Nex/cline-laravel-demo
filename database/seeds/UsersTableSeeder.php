<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(
            ['email' => config('admin.admin_email')], [
                'name' => config('admin.admin_name'),
                'password' => bcrypt(config('admin.admin_password')),
                'role' => 'admin',
            ]
        );
    }
}
