<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Feedback;
use Faker\Generator as Faker;

$factory->define(Feedback::class, function (Faker $faker) {
    return [
        'subject' => substr($faker->sentence(2), 0, -1),
        'message' => $faker->paragraph,
        'client_name' => $faker->name,
        'client_email' => $faker->email,
        'file_url' => $faker->url,
        'is_replied' => 0,
    ];
});
