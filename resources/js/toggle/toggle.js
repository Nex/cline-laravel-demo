$(function() {
    $('.toggle-replied').change(function() {
        const checked = $(this).prop('checked');
        const url = checked ? $(this).data('on-url') : $(this).data('off-url');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
    })
});
