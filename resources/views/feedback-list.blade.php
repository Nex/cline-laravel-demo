@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Заявки</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            @foreach ($feedbacks as $feedback)
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{ $feedback->subject }}</h5> &mdash; <i>{{ $feedback->client_name }} <a href="mailto:{{ $feedback->client_email }}">{{ $feedback->client_email }}</a></i>, {{ $feedback->created_at->format('d.m.Y H:i:s') }}
                        </div>
                        <div class="card-body">
                            <div class="feedback-list-item">
                                <p>{{ $feedback->message }}</p>
                                @if ($feedback->file_url)
                                    <div>
                                        <p><a href="{{ $feedback->file_url }}">{{ $feedback->file_url }}</a></p>
                                    </div>
                                @endif
                                <input
                                        class="toggle-replied"
                                        type="checkbox"
                                        data-toggle="toggle"
                                        {{ $feedback->is_replied ? 'checked' : '' }}
                                        data-on="Ответ отправлен"
                                        data-off="Ответ не отправлен"
                                        data-onstyle="success"
                                        data-offstyle="primary"
                                        value="Не отправлено"
                                        data-on-url="{{ route('feedback-mark-replied', $feedback->id)}}"
                                        data-off-url="{{ route('feedback-mark-not-replied', $feedback->id)}}"
                                        data-width="180"
                                        data-height="37">
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
