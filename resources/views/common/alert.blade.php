@if (session('success'))
    <div class="container">
        <div class="row">
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        </div>
    </div>
@endif
@if (session('error'))
    <div class="container">
        <div class="row">
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
        </div>
    </div>
@endif
