@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Личный кабинет</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>Добро пожаловать!</p>

                    @can('submitFeedback')
                        <a class="btn btn-primary" href="{{ route('feedback-submit') }}">
                            Отправить заявку
                        </a>
                    @endcan

                    @can('listFeedback')
                        <a class="btn btn-primary" href="{{ route('feedback-list') }}">
                            Список заявок
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
