@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Отправьте заявку</h1>
        </div>
        <div class="row">
            <form action="{{ route('feedback-submit') }}" method="post">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Форма заявки содержит обязательные поля. Заполните их, и попробуйте отправить снова
                    </div>
                @endif
                <div class="form-group">
                    <label for="subject">Тема</label>
                    <input type="text" class="form-control @error('subject') is-invalid @enderror" id="subject" name="subject" placeholder="Тема" value="{{ old('subject') }}">
                    @error('subject')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="message">Сообщение</label>
                    <textarea class="form-control @error('message') is-invalid @enderror" id="message" name="message" placeholder="Сообщение">{{ old('message') }}</textarea>
                    @error('message')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Отправить</button>
            </form>
        </div>
    </div>
@endsection