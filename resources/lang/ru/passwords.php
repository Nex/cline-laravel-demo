<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль сброшен!',
    'sent' => 'Мы отправили вам ссылку на сброс пароля!',
    'throttled' => 'Пожалуйста, подождите перед повторной попыткой.',
    'token' => 'У ссылки сброса пароля истёк срок действия.',
    'user' => 'Пользователь с таким Email не найден',

];
