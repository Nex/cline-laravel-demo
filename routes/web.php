<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Обычный пользователь

Route::get('/feedback/submit', 'FeedbackController@viewForm')
    ->middleware('can:submitFeedback')
    ->name('feedback-submit');

Route::post('/feedback/submit', 'FeedbackController@submitForm')
    ->middleware('can:submitFeedback');


// Администратор

Route::get('/feedback/list', 'FeedbackController@list')
    ->middleware('can:listFeedback')
    ->name('feedback-list');

Route::post('/feedback/{id}/mark-replied', 'FeedbackController@markReplied')
    ->middleware('can:replyFeedback')
    ->name('feedback-mark-replied');

Route::post('/feedback/{id}/mark-not-replied', 'FeedbackController@markNotReplied')
    ->middleware('can:replyFeedback')
    ->name('feedback-mark-not-replied');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
