## Установка

1. Клонируем репозиторий.

    ```
    git clone https://Nex@bitbucket.org/Nex/cline-laravel-demo.git cline-laravel-demo
    ```

2. Ставим пакеты композера.

    ```
    cd cline-laravel-demo
    composer install
    ```

3. Устанавливаем файл .env и генерируем ключ приложения.

    ```
    composer run-script init-env
    ```

4. Создаём БД для проекта и прописываем её настройки в конфиге .env в корне проекта.

5. Прогоняем миграции.

    ```
    php artisan migrate --seed
    ```

6. Устанавливаем фронт.

    ```
    npm install
    npm run dev
    ```

7. Запускаем сервер.

    ```
    php artisan serve
    ```

## Использование

http://localhost:8000

Данные для входа менеджера:

Логин `manager@cline-test.local`

Пароль `password`
