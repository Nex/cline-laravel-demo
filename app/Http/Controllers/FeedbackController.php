<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\User;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewForm()
    {
        return view('feedback-submit');
    }

    public function submitForm(Request $request)
    {
        $data = $request->validate([
            'subject' => 'required|max:255',
            'message' => 'required|max:255',
        ]);

        $feedback = new Feedback($data);

        $feedback->is_replied = 0;

        $user = User::first();

        $feedback->client_name = $user->name;
        $feedback->client_email = $user->email;

        if (!$feedback->save()) {
            $request->session()->flash('error', 'Ошибка при отправке заявки');
        } else {
            $request->session()->flash('success', "Заявка \"{$feedback->subject}\" отправлена!");
        }

        return redirect()->route('feedback-submit');
    }

    public function list()
    {
        $feedbacks = Feedback::all();

        return view('feedback-list', ['feedbacks' => $feedbacks]);
    }

    public function markReplied($id)
    {
        return $this->updateReplied($id, 1);
    }

    public function markNotReplied($id)
    {
        return $this->updateReplied($id, 0);
    }

    private function updateReplied($id, int $value)
    {
        $feedback = Feedback::findOrFail($id);

        if ($feedback->is_replied !== $value) {
            $feedback->is_replied = $value;
            $feedback->save();
        }

        return response()->json(['status' => 'success']);
    }
}
